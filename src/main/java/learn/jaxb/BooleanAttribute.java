
package learn.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class BooleanAttribute extends XmlAdapter<String,Boolean> {

    @Override
    public Boolean unmarshal(String v) throws Exception {
        return Boolean.valueOf(v);
    }

    @Override
    public String marshal(Boolean v) throws Exception {
        if (v != null && v) {
            return String.valueOf(v);
        }
        return null;
    }
}
