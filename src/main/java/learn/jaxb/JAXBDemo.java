
package learn.jaxb;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
 
public class JAXBDemo {
 
    public static void main(String[] args) throws Exception {
        StringWriter sw = new StringWriter();
        
        JAXBContext jc = JAXBContext.newInstance(Customer.class);
 
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(Data.CUSTOMER, sw);
        String xml = sw.toString();
        System.out.println(xml);
        
        StringReader sr = new StringReader(xml);
        Unmarshaller un = jc.createUnmarshaller();
        Object customer = un.unmarshal(sr);
        System.out.println("unmarshalled customer="+customer);
        
    }
}