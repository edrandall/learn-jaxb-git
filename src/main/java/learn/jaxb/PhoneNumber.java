

package learn.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType
@XmlAccessorType(XmlAccessType.NONE)
public class PhoneNumber {
 
    private String type;
    private boolean work;
    private Boolean cell;
    private String number;

    public PhoneNumber() {
    }

    @Override
    public String toString() {
        return super.toString()+"{type=" + type + ", work=" + work + ", cell=" + cell + ", number=" + number + '}';
    }

    /**
     * @return the type
     */
    @XmlAttribute
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
        switch(type) {
            case ("work"):
                setWork(true);
                setCell(null);
                break;

            case ("cell"):
                setCell(true);
                setWork(false);
                break;
                
            default:
                setWork(false);
                setCell(null);
                break;
        }
    }

    /**
     * @return the number
     */
    @XmlElement
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }
 
    @XmlElement
    @XmlJavaTypeAdapter(value=BooleanElement.Adapter.class,type=boolean.class)
    public boolean getWork() {
        return work;
    }
    
    public void setWork(boolean work) {
        this.work = work;
    }
    
    
    @XmlElement
    @XmlJavaTypeAdapter(value=BooleanElement.Adapter.class)
    public Boolean getCell() {
        return cell;
    }
    
    public void setCell(Boolean cell) {
        this.cell = cell;
    }

}
