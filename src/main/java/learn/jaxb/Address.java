
package learn.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType
@XmlAccessorType(XmlAccessType.NONE)
public class Address {
 
    private String city;
    private String street;
    private boolean home;

    public Address() {
    }

    @Override
    public String toString() {
        return super.toString()+"{city=" + city + ", street=" + street + ", home=" + home + '}';
    }

    /**
     * @return the city
     */
    @XmlElement
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    @XmlElement
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }
 
    @XmlAttribute(name="home")
    @XmlJavaTypeAdapter(value=BooleanAttribute.class, type=boolean.class)
    public boolean getHome() {
        return home;
    }
    
    public void setHome(boolean home) {
        this.home = home;
    }
}
