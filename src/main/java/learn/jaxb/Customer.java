
package learn.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
 
@XmlRootElement
@XmlType
@XmlAccessorType(XmlAccessType.NONE)
public class Customer {
 
    @XmlAttribute
    private long id;
    
    @XmlElement
    private String name;
    
    @XmlElement
    private Address address;
    
    @XmlElementWrapper(name="phone-numbers")
    @XmlElement(name="phone-number")
    private List<PhoneNumber> phoneNumbers;
 
    public Customer() {
        phoneNumbers = new ArrayList<PhoneNumber>();
    }

    @Override
    public String toString() {
        return super.toString()+"{id=" + id + ", name=" + name + ", address=" + address + ", phoneNumbers=" + phoneNumbers + '}';
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the phoneNumbers
     */
    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    /**
     * @param phoneNumbers the phoneNumbers to set
     */
    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
 
}