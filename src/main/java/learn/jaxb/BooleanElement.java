package learn.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class BooleanElement {

    public static class Adapter extends XmlAdapter<BooleanElement,Boolean> {

        @Override
        public Boolean unmarshal(BooleanElement v) throws Exception {
            return Boolean.TRUE;
        }

        @Override
        public BooleanElement marshal(Boolean v) throws Exception {
            if (v != null && Boolean.TRUE.equals(v)) {
                return new BooleanElement();
            }
            return null;
        }
    }

}
